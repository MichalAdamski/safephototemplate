package com.zpo.project.safephoto.infrastructure.interfaces;

import com.zpo.project.safephoto.domain.Metadata;
import com.zpo.project.safephoto.infrastructure.models.CustomerModel;
import com.zpo.project.safephoto.infrastructure.models.MetadataModel;
import com.zpo.project.safephoto.infrastructure.models.PhotoModel;

public interface ICustomerService {
    CustomerModel parseFromString(String customerString);
    String serializeToString(Metadata metadata);
    String serializeToString(MetadataModel metadata);
    String serializeToString(PhotoModel metadata);
}
