package com.zpo.project.safephoto.infrastructure;

import com.zpo.project.safephoto.domain.interfaces.IPhotoProjector;
import com.zpo.project.safephoto.domain.projections.MetadataProjection;
import com.zpo.project.safephoto.domain.projections.PhotoProjection;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
/***
 * Służy do pobierania projekcji (reprezentacji) encji dla celów odczytu danych, projekcja może różnić się od encji, np. łączyć kilka encji itd. (bazodanowy widok)
 * Projektor to mój sposób nazywania, można to robić różnie
 */
@Repository
public class PhotoProjector implements IPhotoProjector {

    @Override
    public PhotoProjection getById(long id, String token) {
        // tylko dla przykładu, do zmiany!
        return new PhotoProjection("real phoro".getBytes(), new MetadataProjection("","", LocalDateTime.now(),"",""));
    }

    @Override
    public PhotoProjection getManyByCustomerToken(String token) {
        return null;
    }
}
