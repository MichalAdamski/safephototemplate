package com.zpo.project.safephoto.infrastructure.services;

import com.google.gson.Gson;
import com.zpo.project.safephoto.domain.Metadata;
import com.zpo.project.safephoto.infrastructure.interfaces.ICustomerService;
import com.zpo.project.safephoto.infrastructure.models.CustomerModel;
import com.zpo.project.safephoto.infrastructure.models.MetadataModel;
import com.zpo.project.safephoto.infrastructure.models.PhotoModel;
import org.springframework.stereotype.Service;

@Service
public class CustomerJsonService implements ICustomerService {
    @Override
    public CustomerModel parseFromString(String customerString) {
        var gson = new Gson();
        return gson.fromJson(customerString, CustomerModel.class);
    }

    @Override
    public String serializeToString(Metadata metadata) {
        return null;
    }

    @Override
    public String serializeToString(MetadataModel metadata) {
        return null;
    }

    @Override
    public String serializeToString(PhotoModel metadata) {
        return null;
    }
}
