package com.zpo.project.safephoto.domain.snapshots;

import java.time.LocalDateTime;

public class MetadataSnapshot {
    private String title;
    private String location;
    private LocalDateTime date;
    private String description;
    private String format;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getTitle() {
        return title;
    }

    public String getLocation() {
        return location;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getFormat() {
        return format;
    }

    public MetadataSnapshot(String title, String location, LocalDateTime date, String description, String format) {
        this.title = title;
        this.location = location;
        this.date = date;
        this.description = description;
        this.format = format;
    }
}
