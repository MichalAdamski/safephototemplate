package com.zpo.project.safephoto.domain.projections;

import java.time.LocalDateTime;

/**
 * Projekcja to widok, sposób reprezentacji danych domenowych na zewnątrz
 * Może łączyć kilka encji w jedną, dodawać pola jak count itd.
 */
public class MetadataProjection {
    private String title;
    private String location;
    private LocalDateTime date;
    private String description;
    private String format;

    public String getTitle() {
        return title;
    }

    public String getLocation() {
        return location;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getFormat() {
        return format;
    }

    public MetadataProjection(String title, String location, LocalDateTime date, String description, String format) {
        this.title = title;
        this.location = location;
        this.date = date;
        this.description = description;
        this.format = format;
    }
}
